/*
	author: S. M. Shahriar Nirjon
	last modified: Shahrear Iqbal
*/
//there is 1 problem n that is with hitufo
# include "iGraphics.h"

//variables

int tnum=1;//this variable will decide the number of tanks....
int progress=0;//progress is a variable that will continue the steps
int p=0;//checking if progress is 1
int bcall=0;//calling the moving bullet of ufo
int bcount=0;//this will count the number of bullets of ufo
int qountufohit=0;//this wil count the hit of the ufo
int pic_x = 350,pic_y = 400;//pic_x & pic_y are the co ordinate of the ufo
int bulufo_x=350+64,bulufo_y=400+32;// bulufo_x &bulufo_y are the co ordinate of the permanent bullet present in the ufo;
double bullet_r = 10;//radius of bullets
double bullet_x=-1;
double bullet_y=-1;//co ordinate of the bullet that will move
double tankcre[100]={0};//this will store the co ordinates of created tanks
double tbulletcre[100]={0};//this will store the co ordinates of created bullet of tanks
int by=0;//y co ordinate of the tank bullet
int bx;// variable used in hit ufo function
int check[100]={0};//this array will check for each tank if they are hit
/* 
	function iDraw() is called again and again by the system.
*/
void bulletdown();
void hit ();
void tankcreate()//the co ordinate of tanks are created
{
	int k;

	for (k=1;k<tnum+1;k++)
	{
		tankcre[k]=tankcre[k-1]-200;
		//printf("%d ",check[k]);
	}
}
void drawtank ()//the tanks are drawn
{	
	hit();
	int k;
	//printf("In Drawtank\n");
	for (k=0;k<tnum;k++)
	{
		//hit();
	//	printf("%d ",check[k]);
		if (check[k]==0)
			iShowBMP(tankcre[k],0,"tank.bmp");
	}
	//printf("\n");
//	printf("%d\n",tnum);
}
void tankChange()
{
	hit();
	int k;
	//printf("In TankChange %d\n",tnum);
	
	for (k=0;k<tnum;k++)
	{
	//		printf("%d ",check[k]);
		if (check[k]==0)
		tankcre[k] = tankcre[k] + 0.5;
		if(tankcre[k] >512)
		{
			tankcre[k]=15;
		}
		//printf("%lf ",tankcre[k]);
	}
	//printf("\n");
	
}
void tbulletcreate ()//point bullet of tank is created
{
	hit();
	int k;
	for (k=0;k<tnum;k++)
	{
		if (check[k]==0)
		tbulletcre[k]=tankcre[k]+60;
		else 
			tbulletcre[k]=-10;
	}
}
void ub ()//bullet of tank will go upward
{
	by=by++;
	if (by>500)
		by=15;
}
void drawtbullet ()//tank bullet is drawn
{
	hit();
	int k;
	for (k=0;k<tnum;k++)
	{
		iSetColor(0,250,0);
		if (check[k]==0)
			iFilledCircle(tbulletcre[k],by,bullet_r);
	}
	ub();
}
void hitufo ()//bullet will hit ufo
{
	int k;
	double bx;
	for (k=0;k<tnum;k++)
	{	
		bx=tbulletcre[k];
		if ((bx)>=pic_x&&(bx)<(pic_x+128))
		{
			//printf("%lf  %lf\n",bx,pic_x);
			if (by>=pic_y&&by<=pic_y+64)
			{
				//printf("b4   %d\n",qountufohit);
				qountufohit++;
				//printf("aftr   %d\n",qountufohit);
				//bx=-1;by=-1;
			}
		}
	}

}
void hit()
{
	int k;
	double t;
	for (k=0;k<tnum;k++)
	{
		t = tankcre[k];
		if ((bullet_x)>=t && (bullet_x)<=(t+128))
		{
			if ( (bullet_y)>=0 && (bullet_y)<=128)
				check[k]=1;
		}
	
	}
}

void iDraw()
{
	iClear();

	hit();
	if (progress==0)
		iShowBMP(0,0,"bg.bmp");// if progress=0 the opening page will be open
	if (progress==1)// menu bar
	{
		iShowBMP(0,0,"menu.bmp");
	}
	if (progress==5)// level
	{
		iShowBMP(0,0,"level.bmp");
	}
	if (progress==6)// how to play
	{
		iShowBMP(0,0,"how to play.bmp");
	}
	if (progress==4)// load game  need to b finished
	{
		iFilledRectangle(0,0,512,512);
	}
	if (progress==2)// new game
	{
		iClear();
		iSetColor(250,250,250);//white color
		iFilledRectangle(0,0,512,512);
		
		if (qountufohit<2)//the ufo have 3 lives at 4th hit it will vanish
		{
			iSetColor(250,0,0);//the color of bullet of ufo
			iFilledCircle (bulufo_x,bulufo_y,bullet_r,100);//bullet of ufo
			iShowBMP(pic_x,pic_y,"ufo.bmp");//pic of ufo uploaded
			if (bcall==1)
			iFilledCircle (bullet_x,bullet_y,bullet_r,100);//moving bullet of ufo	
		}
		if (qountufohit>50) progress=3;
		tankChange();
		tbulletcreate();//point of bullet of tank is created
		drawtbullet();//bullet of tank is drawn
		drawtank();//tank will be drawn
		hitufo();//the bullet will hit the ufo
	}
	if (progress==3)//end game
	{
		iShowBMP(0,0,"you lost.bmp");
	}
	
}

/* 
	function iMouseMove() is called when the user presses and drags the mouse.
	(mx, my) is the position where the mouse pointer is.
*/
void iMouseMove(int mx, int my)
{
	//place your codes here
}

/* 
	function iMouse() is called when the user presses/releases the mouse.
	(mx, my) is the position where the mouse pointer is.
*/
void iMouse(int button, int state, int mx, int my)
{
	if(button == GLUT_LEFT_BUTTON && state == GLUT_DOWN)
	{
		//place your codes here	
		if (progress==1)
			{
			if (mx>=335 && mx<=493)//for new game
			{	if (my>=251 && my<=426)
					progress=2;
			}
			if (mx>=335 && mx<=493)//this is for load game
			{	if (my>=175 && my<=255)
					progress=4;
			}
			if (mx>=335 && mx<=493)//this is for level
			{	if (my>=7 && my<=83)
					progress=5;
			}
			if (mx>=335 && mx<=493)//this is for how to play
			{	if (my>=90 && my<=169)
					progress=6;
			}
			if (mx>=27 && mx<=151)//this is for exit
			{	if (my>=19 && my<=82)
					exit(0);	
			}
		}
		if (progress==6)
		{
			if (mx>=30 && mx<=151)//return to start menu for how to play
			{	
				if (my>=34 && my<=91)
				progress=1;
			}
		}
		if (progress==5)
		{
			if (mx>=357 && mx<=472)//this is for level 1
			{	
				if (my>=52 && my<=166)
				tnum=1;
			}
			if (mx>=39 && mx<=272)//this is for level 2
			{
				if (my>=206 && my<=322)
				tnum=2;
			}
			if (mx>=239 && mx<=474)//this is for level 3
			{	
				if (my>=52 && my<=166)
				tnum=2;
			}
			if (mx>=39 && mx<=159)//return to start menu
			{	
				if (my>=13 && my<=77)
				progress=1;
			}
		}
	}
	if(button == GLUT_RIGHT_BUTTON && state == GLUT_DOWN)
	{
		//place your codes here	
	}
}

/*
	function iKeyboard() is called whenever the user hits a key in keyboard.
	key- holds the ASCII value of the key pressed. 
*/
void iKeyboard(unsigned char key)
{
	
	if(key == 'a')//the ufo will go left
	{
		if (pic_x>=10)//this will boundary the movement of the ufo
			pic_x= pic_x-4;
		if (bulufo_x>=74)//this will boundary the movement of the permanent bullet of ufo
			bulufo_x=bulufo_x-4;
	}
	if(key == 'd')//the ufo will go right
	{
		if (pic_x+128<=512)//this will boundary the movement of the ufo
			pic_x= pic_x+4;
		if (bulufo_x<=512-64)//this will boundary the movement of the permanent bullet of ufo
			bulufo_x=bulufo_x+4;
	}
	if(key == 'w')//the ufo will go up
	{
		if (pic_y+74<=512)//this will boundary the movement of the ufo
			pic_y= pic_y+4;
		if (bulufo_y<=512-20)//this will boundary the movement of the permanent bullet of ufo
			bulufo_y=bulufo_y+4;
	} 
	if(key == 's')//the ufo wil go down
	{
		if (pic_y>=130)//this will boundary the movement of the ufo
			pic_y= pic_y-4;
		if (bulufo_y>=130+20)//this will boundary the movement of the permanent bullet of ufo
			bulufo_y=bulufo_y-4;
	}

	if (key == 'p')
	{
		if(p==0)
		{
			p=1;		
			progress=1;
		}
	}
	if (key == 'k')
	{
		if (bcount<8)//maximum 8 bullets can be used
		{
			bcall=1;
			bullet_x=bulufo_x;
			bullet_y=bulufo_y;
			bulletdown();
			bcount++;
		}		
	}
	//place your codes for other keys here

}
void bulletdownauto()//this function will cause the bullet of ufo go down
{
	if (bullet_y!=-20)
	bullet_y=bullet_y-5;
}
void bulletdown()//this function will cause the bullet of ufo go down
{
	int brk=0;//break of the bullet of ufo
	if (brk==0)
	{
		iSettimer(20,bulletdownauto);
		if (bullet_y==0)
		{
			brk=1;
		}
	}
}
/*
	function iSpecialKeyboard() is called whenver user hits special keys like-
	function keys, home, end, pg up, pg down, arraows etc. you have to use 
	appropriate constants to detect them. A list is:
	GLUT_KEY_F1, GLUT_KEY_F2, GLUT_KEY_F3, GLUT_KEY_F4, GLUT_KEY_F5, GLUT_KEY_F6, 
	GLUT_KEY_F7, GLUT_KEY_F8, GLUT_KEY_F9, GLUT_KEY_F10, GLUT_KEY_F11, GLUT_KEY_F12, 
	GLUT_KEY_LEFT, GLUT_KEY_UP, GLUT_KEY_RIGHT, GLUT_KEY_DOWN, GLUT_KEY_PAGE UP, 
	GLUT_KEY_PAGE DOWN, GLUT_KEY_HOME, GLUT_KEY_END, GLUT_KEY_INSERT 
*/
void iSpecialKeyboard(unsigned char key)
{
	

	if(key == GLUT_KEY_END)
	{
		exit(0);	
	}
	
	//place your codes for other keys here
}

int main()
{
	//freopen("d:\\output.txt","wt",stdout);
	
	tankcre[0]=50;
	tankcreate();
	//printf("%d\n",qountufohit);
	//place your own initialization codes here. 
	iInitialize(512, 512 ,"devils destroys the world");
	
	return 0;
}	